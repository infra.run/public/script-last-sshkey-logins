#!/usr/bin/env bash
# Description: Simple Script to Match last used SSH-Key's to Owner
# Author: Sascha 'cascha' Behrendt
# Date: 2020

LAST=3 # Show last 3 Logins

HOME=$(getent passwd $USER | cut -d: -f6)
KEYFILE="$HOME/.ssh/authorized_keys"
AUTHLOG="/var/log/auth.log"
CONFIGFILE="/etc/ssh/sshd_config"
i=1

function check_loglevel {
	IS_VERBOSE=$(grep -si "LogLevel VERBOSE" "$CONFIGFILE";)
	if [[ "${IS_VERBOSE,,}" == "loglevel verbose" ]]; then
		:
	else
		echo -e "\nWrong Loglevel. set 'LogLevel' to 'VERBOSE' in $CONFIGFILE, aborting.\n"
		exit 0
	fi
}

function check_keyfile {
	if [[ -r "$KEYFILE" ]]; then
		:
	else
		echo -e "\n$KEYFILE not found, aborting.\n"
		exit 0
	fi
}

function check_authlog {
	if [[ -r "$AUTHLOG" ]]; then
		:
	else
		echo -e "\n$AUTHLOG not found, aborting.\n"
		exit 0
	fi
}

function show_last_logins {
	echo -e "\n$(tput bold)Last $LAST Logins:$(tput sgr0)"
	grep -sB 1 "Postponed publickey for $USER" $AUTHLOG | tail -n $((LAST * 3))

	echo -e "\n$(tput bold)Pubkeys found:$(tput sgr0)"
	awk '!/^$/ {print $(NF)}' $KEYFILE | while read line; do echo "$i: $line"; i=$((i + 1)); done
	echo ""
}

check_loglevel
check_keyfile
check_authlog
show_last_logins
